const { app, BrowserWindow } = require('electron')

const defaultURL = 'https://tasks.google.com/embed/?origin=https://calendar.google.com&fullWidth=1'

function createWindow () {
  const win = new BrowserWindow({
    width: 600,
    height: 700,
    webPreferences: {
      nodeIntegration: true
    },
    icon: 'app.icns'
  })

  win.loadURL(defaultURL)
  win.webContents.on('will-navigate',(event) => {
      console.log('Ready', win.webContents.getURL());
      event.preventDefault()
      win.loadURL(defaultURL)
  });

  console.log("Launched") 
}

app.userAgentFallback =  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) old-airport-include/1.0.0 Chrome Electron/7.1.7 Safari/537.36"
app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})